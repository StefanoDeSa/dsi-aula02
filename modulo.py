def valida_entrada(entrada):
    entrada_str = str(entrada)
    valores_romanos = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}

    entrada_str = entrada_str.upper()
    total = 0
    valor_anterior = 0

    
    for algarismo in reversed(entrada_str):
        if algarismo not in valores_romanos:
            return False
        valor_atual = valores_romanos[algarismo]

        if valor_atual >= valor_anterior:
            total += valor_atual
        else:
            total -= valor_atual

        valor_anterior = valor_atual

    return entrada_str

def romano_to_int(s):
    # Mapeamento dos algarismos romanos para valores
    roman_dict = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
    
    # Inicializa o resultado
    result = 0
    
    # Percorre a string romana da esquerda para a direita
    for i in range(len(s)):
        # Se o valor numérico do algarismo atual for menor que o próximo, subtrai
        if i < len(s) - 1 and roman_dict[s[i]] < roman_dict[s[i+1]]:
            result -= roman_dict[s[i]]
        else:
            result += roman_dict[s[i]]
    
    return print(f'Valor: {result}')
